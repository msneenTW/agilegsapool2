﻿function locationName(obj) {
    return obj.city + ", " + obj.state;
}

function getLocationBlock(value) {
    return $("span.cityName:contains('" + value + "')").parents(".location");
}

function getGridRow(value) {
    return $(".ui-grid-cell-contents:contains('" + value + "')");
}

function joinFirmNames(parent) {
    return $.unique(parent.find(".recallFirm").map(function () {
        return $(this).text();
    }).get()).join("; ");
}

var Map = {
    defaults: {
        center: new google.maps.LatLng(38.4987789, -98.035841),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    },
    el: null,
    instance: null,
    isPrinting: false,
    markers: [],
    locations: [],
    data: [],
    index: 0,
    delay: 100,
    openWindow: {},
    initialize: function (el) {
        var me = this;
        this.el = el;
        this.instance = new google.maps.Map(el, this.defaults);
        this.geocoder = new google.maps.Geocoder();
    },
    displayAddress: function () {
        var me = this;
        if (me.index < me.locations.length) {
            setTimeout(function () {
                me.codeAddress(me.locations[me.index]);
            }, me.delay);
        }
    },
    codeAddress: function (address) {
        //var obj = ""
        var me = this;
        this.geocoder.geocode({
            'address': address
        }, function (response, status) {
            switch (status) {
                case google.maps.GeocoderStatus.OK:
                    var location = response[0].geometry.location;
                    var parentBlock = getLocationBlock(address);
                    var marker = new google.maps.Marker({
                        position: location,
                        title: joinFirmNames(parentBlock)
                    });

                    marker.setMap(me.instance);

                    var infobox = new google.maps.InfoWindow({
                        content: parentBlock.html()
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        if (me.openWindow.close != undefined) me.openWindow.close();
                        me.openWindow = infobox;
                        infobox.open(me.instance, this);
                    });

                    me.markers.push(marker);

                    getGridRow(address).css({ cursor: "pointer" }).on("click", function () {
                        var idx = $.inArray($(this).text(), Map.locations);
                        if (idx !== -1) {
                            google.maps.event.trigger(Map.markers[idx], 'click');
                        }
                    });
                    me.index++;
                    break;
                case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
                    me.delay++;
                    break;
                default:
                    alert("Geocode was not successful for the following reason: " + status);
                    break;
            }
            me.displayAddress();
        });
    },

    clear: function () {
        this.clearMarkers();
    },
    clearMarkers: function () {
        for (var i = 0, len = this.markers.length; i < len; i++) {
            var marker = this.markers[i];
            marker.setMap(null);
        }
        this.markers = [];
        return this;
    },
    start: function (data) {
        var me = this;
        me.clearMarkers();
        me.data = data;
        me.index = 0;
        me.locations = $.unique($.map(data.results, function (value) {
            return value.city + ', ' + value.state;
        }));
        me.displayAddress();
    }
};


var app = angular.module("app", ["ui.grid", "angular.filter"])
    .filter('checkClass', function () {
        return function (object) {
            return "classification" + object.split(" ")[1].length.toString();
        };
    });

app.controller("gridCtrl", function ($scope, $http) {
    $scope.currentPage = 1;
    $scope.gridOptions = {
        enableColumnMenus: false,
        columnDefs: [
            { field: "recall_number" },
            { field: "recalling_firm" },
            {
                field: "classification", cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                    var value = "classification" + grid.getCellValue(row, col).split(" ")[1].length.toString();
                    return value;
                }
            },
            { field: "getLocation()", displayName: "City, State" },
            { field: "status" }
        ]
    };

    $scope.previousPage = function () {
        $scope.currentPage--;
        $scope.getFoodData();
    }

    $scope.nextPage = function () {
        $scope.currentPage++;
        $scope.getFoodData();
    }

    $scope.reset = function () {
        $scope.gridOptions.data = $scope.foodData;
        angular.forEach($scope.foodData, function (row) {
            row.getLocation = function () {
                return locationName(this);
            }
        });
    }

    $scope.locationCount = function () {
        $http.get("/api/FdaFood/LocationCount")
            .success(function (d) {
                $scope.foodCounts = d;
            });
    }

    $scope.getFoodData = function () {
        $scope.currentPage = Math.max(1, $scope.currentPage);
        $http.get("/api/FdaFood/" + $scope.currentPage)
            .success(function (d) {
                var results = [];
                $.each(d.results, function (i, v) {
                    results.push($.extend(v, { city: v.city.toUpperCase(), state: v.state.toUpperCase() }));
                });
                $scope.foodData = results;
                $scope.reset();
                $scope.pages = Math.ceil(d.meta.results.total / 25);
                Map.start(d);
            });
    }

    $scope.groupCityState = function (object) {
        return locationName(object);
    }

    Map.initialize($('#map')[0]);
    $scope.getFoodData();
});
angular.bootstrap(document, ['app']);
